<?php

$router->get('/', ['App\Controllers\HomeController', 'show']);
$router->get('/privacy', ['App\Controllers\PrivacyController', 'privacy']);
$router->get('/cookie', ['App\Controllers\PrivacyController', 'cookie']);
$router->get('/form', ['App\Controllers\HomeController', 'edit']);
$router->post('/grazie', ['App\Controllers\MailController', 'send']);
