$(document).foundation();

$(document).ready(function(){

  setTimeout(function(){
    $('body').animate({
      opacity: 1
    }, 400);
  }, 600);

  $.cookieBar({
      message: 'Questo sito utilizza cookie tecnici e di terze parti per garantirti una migliore esperienza di navigazione. Se prosegui nella navigazione assumiamo che tu ne sia consapevole.',
      acceptButton: true,
      acceptText: 'Ok, ho capito',
      acceptFunction: null,
      declineButton: false,
      declineText: 'Non acconsento',
      declineFunction: null,
      policyButton: true,
      policyText: 'Cookie Policy',
      policyURL: 'https://www.iubenda.com/privacy-policy/685066/cookie-policy',
      autoEnable: true,
      acceptOnContinue: false,
      acceptOnScroll: 1,
      acceptAnyClick: true,
      expireDays: 365,
      renewOnVisit: false,
      forceShow: false,
      effect: 'slide',
      element: 'body',
      append: false,
      fixed: true,
      bottom: false,
      zindex: '9',
      domain: 'www.sitiinternetalberghi.it',
      referrer: 'www.nodopiano.it'
  });

  // Custom accept on scroll
  $(window).on('scroll', function(){
    var scroll = $(this).scrollTop();
    if(scroll > 200){
      $('.cb-enable').trigger('click');
    }
  });

  $('.cb-enable').on('click', function(){
      if (typeof hotjar == 'function') { 
        hotjar(); 
      }

      if (typeof facebookpixel == 'function') { 
        facebookpixel(); 
      }
      
      if (typeof analytics == 'function') { 
        analytics(); 
      }
      if ($('#map').length){
        initMap();
      }
  });

  $('.np-anchor').click(function() {
    var destination = $(this).attr('href');
    var spostamento = $(destination).offset().top;
    $('html, body').animate({
      scrollTop: spostamento-60
    }, 600, 'swing');
  });

  //Google reCapthcha required:
  $('#submit').click(function(e){
    //Controllo se il reCaptcha è stato convalidato
    if (grecaptcha.getResponse() === "") {
      e.preventDefault();
      $('.robot').fadeIn(400);
    }
    else {
        $('.robot').fadeOut(400);
    }
  });
});
