<?php
Namespace App\Controllers;

use Nodopiano\Corda\Controller;
use Nodopiano\Corda\Mail;
use Nodopiano\Corda\NewsLetter;
use Nodopiano\Corda\Csv;

/**
 *
 */
class MailController extends Controller
{

    public function send() {
      // Csv::export(array($_POST['from'],$_POST['message']));
        // Array di campi
        $merge_vars = array('FNAME' => $_POST['nome']);
        // Iscrizione newsletter
        NewsLetter::subscribe('bff354fe75', array('email' => $_POST['email']), $merge_vars);
    	
        $messaggio = '<br/>'.$_POST['messaggio'].'<br/><br/>'.$_POST['nome'].'<br/><b>Landing: </b>'.$_POST['landing'];
        $mail = Mail::send($_POST['email'],'Nodopiano Landing strutture ricettive - Richiesta informazioni', $messaggio);

        $data = $this->api->pages(getenv('API_PAGE_ID'));
        if ($mail)  return view('thankyou.html', array('data' => $data, 'versione' => $_POST['landing'], 'long_message' => 'Grazie! Il tuo messaggio è stato inviato correttamente.<br/>Riceverai una risposta al più presto.', 'message' => 'Grazie!' ));
        else return view('thankyou.html', array('data' => $data ,'long_message' => "Siamo spiacenti.<br/>Si è verificato un errore durante l'invio del messaggio, riprova più tardi.", 'message' => 'Errore!' ));

    }
}
