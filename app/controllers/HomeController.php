<?php
Namespace App\Controllers;

use Nodopiano\Corda\Controller;

class HomeController extends Controller
{

  public function show() 
  {
    $test = $_GET['test'];
    $ab_testing = $test ?: rand(1, 2);

    // Versione A
    if($ab_testing == 1) {
      $data = $this->api->pages(getenv('API_PAGE_ID'));
    }

    // Versione B
    if($ab_testing == 2) {
      $data = $this->api->pages(2924);
    }

    return view('index.html',array('data' => $data, 'versione' => $ab_testing, 'google_api' => 'AIzaSyAVaG_sfpL-tOLnvNwLF04Zt-o2fVaxhfI'));
  }

  public function page($id)
  {
    return json($this->api->pages($id));
  }

  public function edit()
  {
    return view('form.html', array('message' => 'Hello!'));
  }
}
